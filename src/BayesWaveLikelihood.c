#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "BayesWave.h"
#include "BayesLine.h"
#include "BayesWaveIO.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveLikelihood.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* ********************************************************************************** */
/*                                                                                    */
/*                             Likelihood Functions                                   */
/*                                                                                    */
/* ********************************************************************************** */

static void max_array_element(double *max, int *index, double *array, int n)
{
   int i;

   *max = array[0];
   *index = 0;

   for(i = 1; i < n; i++)
   {
      if(array[i] > *max)
      {
         *max = array[i];
         *index = i;
      }
   }
}

void phase_blind_time_shift(double *corr, double *corrf, double *data1, double *data2, double *psd, int n)
{
   int nb2, i, l, k;

   nb2 = n / 2;

   corr[0] = 0.0;
   corr[1] = 0.0;
   corrf[0] = 0.0;
   corrf[1] = 0.0;

   for (i=1; i < nb2; i++)
   {
      l=2*i;
      k=l+1;

      corr[l]	=  ( data1[l]*data2[l] + data1[k]*data2[k]) / psd[i];
      corr[k]	= -( data1[k]*data2[l] - data1[l]*data2[k]) / psd[i];
      corrf[l] =  ( data1[l]*data2[k] - data1[k]*data2[l]) / psd[i];
      corrf[k] = -( data1[k]*data2[k] + data1[l]*data2[l]) / psd[i];
       
   }


   drealft(corr-1, n, -1);
   drealft(corrf-1, n, -1);

}

double loglike(int imin, int imax, double *r, double Sn, double *invSnf)
{
   /*
    logL = -(r|r)/2 - Nlog(Sn)
    */
   return -0.5*(fourier_nwip(imin, imax, r, r, invSnf)/Sn) - (double)(imax-imin)*log(Sn);
}

double loglike_stochastic(int NI, int imin, int imax, double **r, double ***invCij, double *detC)
{

  int i,j;
  double logL = 0.0;

  for(i=0; i<NI; i++)
  {
    for(j=0; j<NI; j++)
    {
      logL += -0.5*(fourier_nwip(imin, imax, r[i], r[j], invCij[i][j]));
    }
  }
  for(i=imin; i<imax; i++)
  {
    //logL -= log(detC[i]); // this should have a square-root, no?
    logL -= log(detC[i]);
  }
  return logL;
}

double loglike_maxNET(struct Data *data, struct Prior *prior, double **sNET, double **hNET, double **SnfNET, double **invSnfNET, double *SnNET, double t0, double *dt, double *dphi, double *dA)
{
   int N = data->N;
   int halfN = N/2;
   int NI = data->NI;
   int imin = data->imin;
   int imax = data->imax;
   double Tobs = data->Tobs;

   int tmin = (int)floor(prior->range[0][0]/(Tobs/(double)N));
   int tmax = (int)floor(prior->range[0][1]/(Tobs/(double)N));

   int i;
   int ifo;

   double logL;
   double norm, delt, pshift;


   //Chi^2 = (s-h|s-h)/Sn
   double *h = NULL;
   double *s = NULL;
   double *Snf   = NULL;
   double *invSnf = NULL;
   double Sn;

   double rhoNET = 0.0;
   double SSNET  = 0.0;
   double HSNET  = 0.0;

   logL = 0.0;
   for(ifo=0; ifo<NI; ifo++)
   {
      h   = hNET[ifo];
      s   = sNET[ifo];
      invSnf = invSnfNET[ifo];
      Sn  = SnNET[ifo];
      rhoNET += fourier_nwip(imin,imax,h,h,invSnf)/Sn;
      SSNET  += fourier_nwip(imin,imax,s,s,invSnf)/Sn;

   }

   //Create PSD scaled by Sn parameter for Sum_Extreme
   double **Snf_scaled = dmatrix(0,NI-1,0,halfN-1);
   for(ifo=0; ifo<NI; ifo++) for(i=0; i<halfN; i++) Snf_scaled[ifo][i] = SnfNET[ifo][i]*SnNET[ifo];

   HSNET = Sum_Extreme(sNET, hNET, Snf_scaled, N, &delt, &pshift, Tobs, NI, tmin, tmax, t0);
   free_dmatrix(Snf_scaled,0,NI-1,0,halfN-1);

   norm = HSNET/rhoNET;

   logL = -0.5*(SSNET - HSNET*HSNET/rhoNET);

   //Amplitude
   *dA = norm;

   //Time
   *dt = delt;

   //Phase
   *dphi = pshift;

   for(ifo=0; ifo<NI; ifo++) logL -= (double)(imax-imin)*log(SnNET[ifo]);

   if(logL!=logL)
   {
      printf("encountered nan in loglike_maxNet:\n");
      for(ifo=0; ifo<NI; ifo++)printf("     SnNET[%i]=%g\n",ifo,SnNET[ifo]);
      printf("     HSNET = %g\n",HSNET);
      printf("     SSNET = %g\n",SSNET);
      printf("     rhoNET = %g\n",rhoNET);
      FILE *dump = fopen("loglikedump.dat","w");

      for(i=imin; i<imax; i++)
      {
         fprintf(dump,"%i ",i);
         for(ifo=0; ifo<NI; ifo++)
         {
            h   = hNET[ifo];
            s   = sNET[ifo];
            Snf = SnfNET[ifo];
            Sn  = SnNET[ifo];

            fprintf(dump,"%.12g %.12g %.12g %.12g %.12g ",h[i*2],h[i*2+1],s[i*2],s[i*2+1],Snf[i]);
         }
         fprintf(dump,"\n");
      }
      fclose(dump);
      abort();
   }
   return logL;
}

double Sum_Extreme(double **a, double **b, double **Sn, int n, double *delt, double *pshift, double Tobs, int NI, int imin, int imax, double t0)
{
   double max=0.0;
   int i, j;
   int index = 0;
   double **AC, **AF;
   double *corr;
   double *corrcos, *corrsin;

   double srate = (Tobs/(double)n);
   int newtime = n;
   int oldtime = (int)floor(t0/srate);

   // this version simultaneously maximizes across the Network

   AC=dmatrix(0,NI-1,0,n-1);
   AF=dmatrix(0,NI-1,0,n-1);
   corr = dvector(0,n-1);
   corrcos = dvector(0,n-1);
   corrsin = dvector(0,n-1);

   for(i = 0; i < n; i++)
   {
      corr[i] = 0.0;
      corrcos[i] = 0.0;
      corrsin[i] = 0.0;
   }

   for(i = 0; i < NI; i++)  phase_blind_time_shift(AC[i], AF[i], a[i], b[i], Sn[i], n);

   for(j = 0; j < NI; j++)
   {
      for(i = 0; i < n; i++)
      {
         corrcos[i] += AC[j][i];
         corrsin[i] += AF[j][i];
      }
   }
   for(i = 0; i < n; i++) corr[i] = sqrt(corrcos[i]*corrcos[i]+corrsin[i]*corrsin[i]);

   while(newtime<imin || newtime >= imax)
   {
      //TODO: Might be worth sorting corr[] in Sum_Extreme()
      max_array_element(&max, &index, corr, n);

      if(index < (n/2)-1)
         *delt = ((double) index)/((double) n)*Tobs;
      else if(index >= n/2)
         *delt = ((double) (index - n))/((double) n)*Tobs;

      newtime = oldtime + (*delt)/srate;
      corr[index]=0.0;
   }
   max = 2.0*(max);
   *pshift = atan2(corrsin[index],corrcos[index]);

   free_dmatrix(AC, 0,NI-1,0,n-1);
   free_dmatrix(AF, 0,NI-1,0,n-1);
   free_dvector(corr, 0,n-1);
   free_dvector(corrcos, 0,n-1);
   free_dvector(corrsin, 0,n-1);

   return(max);
}

void recompute_residual(struct Data *data, struct Model **model, struct Chain *chain)
{
  int i,j;
  int ic;
  int ifo;
  int NI = data->NI;
  int N  = data->N;
  int NC = chain->NC;
  struct Model *model_x = NULL;

  for(ic=0; ic<NC; ic++)
  {
    model_x = model[chain->index[ic]];

    // re-do residual calculation occasionally to counter any round-off error from delta updates
    for(i=0; i< N; i++)
    {
      model_x->signal->templates[i] = 0.0;
      for(ifo=0; ifo<NI; ifo++) model_x->glitch[ifo]->templates[i] = 0.0;
    }

    if(data->signalFlag)
    {
      for(j=1; j<=model_x->signal->size; j++)
      {
        i = model_x->signal->index[j];
        model_x->wavelet(model_x->signal->templates, model_x->signal->intParams[i], N, 1, data->Tobs);
      }

      computeProjectionCoeffs(data, model_x->projection, model_x->extParams, data->fmin, data->fmax);
      waveformProject(data, model_x->projection, model_x->extParams, model_x->response, model_x->signal->templates, data->fmin, data->fmax);

    }

    if(data->glitchFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        for(j=1; j<=model_x->glitch[ifo]->size; j++)
        {
          i = model_x->glitch[ifo]->index[j];
          model_x->wavelet(model_x->glitch[ifo]->templates, model_x->glitch[ifo]->intParams[i], N, 1, data->Tobs);
        }
      }
    }

    //recompute likelihood
    for(ifo=0; ifo<NI; ifo++)
    {
      for(i=0; i<data->N; i++)
      {
        data->r[ifo][i] = data->s[ifo][i];
        if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
        if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      }
    }

    model_x->logL = 0.0;
    model_x->logLnorm = 0.0;
    if(!data->constantLogLFlag)
    {
      if(data->stochasticFlag)
      {
        //TODO: No support for glitch model in stochastic likelihood
        ComputeNoiseCorrelationMatrix(data, model_x->Snf, model_x->Sn, model_x->background);
        model_x->logL = loglike_stochastic(data->NI, data->imin, data->imax, data->r, model_x->background->Cij, model_x->background->detCij);
      }
      else
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          model_x->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model_x->Sn[ifo], model_x->invSnf[ifo]);
          model_x->logL += model_x->detLogL[ifo];
          for(i=0; i<data->N/2; i++)
          {
            model_x->logLnorm -= log(model_x->Snf[ifo][i]);
          }

        }
      }
    }


  }//end loop over chains
}

double EvaluateSearchLogLikelihood(int typ, int ii, int det, struct Model *mx, struct Model *my, struct Prior *prior, struct Chain *chain, struct Data *data)
{
   int i, ifo;
   double logLy = 0.0;
   double dt,dphi,dA;
   double fmin,fmax;

   //Unpack model_ & data_parameter structures
   int N  = data->N;
   int NI = data->NI;
   int imin = data->imin;
   int imax = data->imax;
    int NW = data->NW;

   double Tobs  = data->Tobs;
   double **r   = data->r;
   double **s   = data->s;
   double **invSnf = mx->invSnf;

   //work-space vectors
   double *h   = data->h;
   double **rh = data->rh;

   for(i=0; i<N; i++)
   {
      h[i]=0.0;
      for(ifo=0; ifo<NI; ifo++) rh[ifo][i]=0.0;
   }
   //pointer to model being varied (glitch or signal)
   struct Wavelet *wave_x = NULL;
   struct Wavelet *wave_y = NULL;
   if(det == -1)
   {
      wave_x = mx->signal;
      wave_y = my->signal;
   }
   else
   {
      wave_x = mx->glitch[det];
      wave_y = my->glitch[det];
   }

   //current model
   double **rx = mx->response;

   double *tx      = wave_x->templates;
   double *paramsx = wave_x->intParams[ii];

   //proposed model
   double *Sny = my->Sn;
   double **ry = my->response;

   int sy          = wave_y->size;
   double *ty      = wave_y->templates;
   double *paramsy = wave_y->intParams[ii];


   //short-cut pointer to glitch models
   double **gx = NULL;
   double **gy = NULL;
   gx = malloc(NI*sizeof(double*));
   gy = malloc(NI*sizeof(double*));

   for(ifo=0; ifo<NI; ifo++)
   {
      gx[ifo] = mx->glitch[ifo]->templates;
      gy[ifo] = my->glitch[ifo]->templates;
   }

   // Rejection sample on prior range
   if(wave_y->size>0 )
   {
      if(checkrange(paramsy,prior->range,NW) )
      {
         free(gx);
         free(gy);
         return -1.0e60;
      }
   }

   // copy over current multi-template & current residual

   for(i=0; i<N; i++)

      for(ifo=0; ifo<NI; ifo++)
      {
         if(ifo==0)
         {
            for(i=0; i<N; i++)
            {
               ty[i] = tx[i];
               ry[ifo][i] = gy[ifo][i] = 0.0;
               r[ifo][i] = s[ifo][i];
            }
         }
         else {
            for(i=0; i<N; i++)
            {
               ry[ifo][i] = gy[ifo][i] = 0.0;
               r[ifo][i] = s[ifo][i];
            }
         }
         if(mx->signal->size>0)
         {
            for(i=0; i<N; i++)
            {
               ry[ifo][i] = rx[ifo][i];
               r[ifo][i] -= ry[ifo][i];
            }
         }
         if(mx->glitch[ifo]->size>0)
         {
            for(i=0; i<N; i++)
            {
               gy[ifo][i] = gx[ifo][i];
               r[ifo][i] -= gy[ifo][i];
            }
         }
      }
   switch(typ)
   {
         // updating a single wavelet
      case 1:

         // we are in the noise-only model - can't use the maximized likelihood
         if(sy == 0)
         {
            for(ifo=0; ifo<NI; ifo++)
            {
               my->detLogL[ifo] = loglike(imin, imax, r[ifo], Sny[ifo], invSnf[ifo]);
               logLy += my->detLogL[ifo];//loglike(imin, imax, r[ifo], Sny[ifo], Snf[ifo]);
            }

            free(gx);
            free(gy);

            return logLy;
         }
         else
         {
            // remove old template
            mx->wavelet(ty, paramsx, N, -1, Tobs);
            if(det==-1)
            {
               mx->wavelet_bandwidth(paramsx,&fmin,&fmax);

               //Make sure frequency is in range
               if(fmin < data->fmin)
                  fmin = data->fmin;
               if(fmax > data->fmax)
                  fmax = data->fmax;

               waveformProject(data, my->projection, my->extParams, ry, ty, fmin, fmax);
            }
            // form up residual using all other wavelets
            for(ifo=0; ifo<NI; ifo++)
            {
               for(i=0; i< N; i++)
               {
                  r[ifo][i] = s[ifo][i];//-ry[ifo][i]-gy[ifo][i];
                  if(data->signalFlag) r[ifo][i] -= ry[ifo][i];
                  if(data->glitchFlag) r[ifo][i] -= gy[ifo][i];
               }
            }
            // generate the new template
            mx->wavelet(h, paramsy, N, 0, Tobs);

            if(det==-1)
            {
               mx->wavelet_bandwidth(paramsy,&fmin,&fmax);

               //Make sure frequency is in range
               if(fmin < data->fmin)
                  fmin = 0.0;
               if(fmax > data->fmax)
                  fmax = data->fmax;

               waveformProject(data, my->projection, my->extParams, rh, h, fmin, fmax);
            }
            else for(i=0; i<N; i++) rh[det][i]=h[i];
         }
         break;

         // adding a new wavelet
      case 2:

         // generate the new template
         mx->wavelet(h, paramsy, N, 0, Tobs);

         if(det==-1)
         {
            /* Delayed Rejection for extrinsic parameters??
             if(chain->burn)
             {
             long seed = -(int)time(NULL);
             struct Model *temp = malloc(sizeof(struct Model));
             initialize_model(temp, data, prior, &seed);

             copy_model(my, temp, N);

             if(sy==1)extrinsic_uniform_proposal(&seed,temp->extParams);

             //add new template to model
             for(i=0; i<N; i++) temp->signal->templates[i] += h[i];

             EvolveExtrinsicParameters(data, prior, temp, chain, &seed, 100, 0);

             for(i=0; i<NE; i++) my->extParams[i] = temp->extParams[i];

             for(ifo=0; ifo<NI; ifo++)
             {
             my->projection->Fplus[ifo]  = temp->projection->Fplus[ifo];
             my->projection->Fcross[ifo] = temp->projection->Fcross[ifo];
             for(i=0; i<N; i++)
             {
             my->projection->expPhase[ifo][i] = temp->projection->expPhase[ifo][i];
             }
             }

             free_model(temp, data, prior);
             free(temp);

             }*/
            mx->wavelet_bandwidth(paramsy,&fmin,&fmax);
            //Make sure frequency is in range
            if(fmin < data->fmin)
               fmin = 0.0;
            if(fmax > data->fmax)
               fmax = data->fmax;

            waveformProject(data, my->projection, my->extParams, rh, h, fmin, fmax);
         }
         else for(i=0; i<N; i++) rh[det][i]=h[i];

         break;

         //remove existing wavelet
      case 3:
         logLy = EvaluateMarkovianLogLikelihood(typ, ii, det, mx, my, prior, chain, data);
         break;

      default:
         break;
   }

   //If likelihood for new wavelet needs to be evaluated (either by birth or move)
   if(typ!=3)
   {
      // note the the residual here is the signal minus all the other SGs
      logLy = loglike_maxNET(data, prior, r, rh, mx->Snf, mx->invSnf, Sny, paramsy[0], &dt, &dphi, &dA);

      //Amplitude
      paramsy[3] *= dA;

      //Time
      paramsy[0] += dt;

      //Phase
      paramsy[4] += dphi;

      //    printf("%g :",2.-paramsy[0]);
      //    FILE *prop=fopen("prop.dat","a");
      //    for(i=0;i<5;i++)fprintf(prop," %g ",paramsy[i]);fprintf(prop,"\n");
      //    fclose(prop);
      //abort();

      if(paramsy[4] < 0.0)       paramsy[4] += LAL_TWOPI;
      if(paramsy[4] > LAL_TWOPI) paramsy[4] -= LAL_TWOPI;

      if(paramsy[0] < 0.0)  paramsy[0] += Tobs;
      if(paramsy[0] > Tobs) paramsy[0] -= Tobs;

      // check that we haven't mapped out of range
      if(wave_y->size>0)
      {
         if(checkrange(paramsy,prior->range,NW))
         {
            free(gx);
            free(gy);
            return -1.0e60;
         }
      }
      // now include the newly proposed SG in the proposed combined template
      // note that we can't just use h since the parameters get updated by the maximization procedure
      mx->wavelet(ty, paramsy, N, 1, Tobs);

      if(det==-1)
      {
         mx->wavelet_bandwidth(paramsy,&fmin,&fmax);

         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = 0.0;
         if(fmax > data->fmax)
            fmax = data->fmax;

         waveformProject(data, my->projection, my->extParams, ry, ty, fmin, fmax);
      }
   }

   free(gx);
   free(gy);

   return logLy;
}

double EvaluateConstantLogLikelihood(int typ, int ii, int det, UNUSED struct Model *mx, struct Model *my, struct Prior *prior, UNUSED struct Chain *chain, UNUSED struct Data *data)
{
   // Rejection sample on prior range
   struct Wavelet *wave = NULL;

   if(det==-1) wave = my->signal;
   else        wave = my->glitch[det];

   /*
    type 3 is a death move -- params[ii] is out of the picture
    and doesn't need to be checked
    */
   if(typ!=3 && wave->size > 0)
   {
      if( checkrange(wave->intParams[ii],prior->range,data->NW)) return -1.0e60;
   }

   return 0.0;
}

double EvaluateMarkovianLogLikelihood(int typ, int ii, int det, struct Model *mx, struct Model *my, struct Prior *prior, struct Chain *chain, struct Data *data)
{
   int i,j,k,ifo,jj;
   double logLy = 0.0;
   double logLseg = 0.0;
   double fmin=data->fmin;
   double fmax=data->fmax;
   double fi,ff;

   //Unpack model_ & data_parameter structures
   int N  = data->N;
   int NI = data->NI;
   int imin = data->imin;
   int imax = data->imax;
   int jmin = imin;
   int jmax = imax;
    int NW = data->NW;

   double **r   = data->r;
   double **s   = data->s;
   double Tobs  = data->Tobs;

  double **invSnf = mx->invSnf;

   //pointer to model being varied (glitch or signal)
   struct Wavelet *wave_x = NULL;
   struct Wavelet *wave_y = NULL;
   if(det == -1)
   {
      wave_x = mx->signal;
      wave_y = my->signal;
   }
   else
   {
      wave_x = mx->glitch[det];
      wave_y = my->glitch[det];
   }

   //current model
   double **rx = mx->response;

   double *tx      = wave_x->templates;
   double *paramsx = wave_x->intParams[ii];

   //proposed model
   double *Sny = my->Sn;
   double **ry = my->response;

   int sy          = wave_y->size;
   double *ty      = wave_y->templates;
   double *paramsy = wave_y->intParams[ii];

   //short-cut pointer to glitch models
   double **gx = NULL;
   double **gy = NULL;
   gx = malloc(NI*sizeof(double*));
   gy = malloc(NI*sizeof(double*));

   for(ifo=0; ifo<NI; ifo++)
   {
      gx[ifo] = mx->glitch[ifo]->templates;
      gy[ifo] = my->glitch[ifo]->templates;

      my->detLogL[ifo] = mx->detLogL[ifo];

   }

   // Rejection sample on prior range
   /*
    type 3 is a death move -- params[ii] is out of the picture
    and doesn't need to be checked
    */
   if( (data->signalFlag || data->glitchFlag) && typ!=3 && wave_y->size>0 )
   {
      if(checkrange(paramsy,prior->range,NW))
      {
         free(gx);
         free(gy);
         return -1.0e60;
      }
   }

   // copy over current multi-template
   for(ifo=0; ifo<NI; ifo++)
   {
      if(ifo==0){
         for(i=0; i<N; i++)
         {
            ty[i] = tx[i];
            r[ifo][i]  = s[ifo][i];
         }
      }
      else{
         for(i=0; i<N; i++)
         {
            r[ifo][i]  = s[ifo][i];
         }
      }
      if(mx->glitch[ifo]->size>0)
      {
         for(i=0; i<N; i++)
         {
            gy[ifo][i] = gx[ifo][i];
            r[ifo][i] -= gx[ifo][i];
         }
      }
      if(mx->signal->size>0)
      {
         for(i=0; i<N; i++)
         {
            ry[ifo][i] = rx[ifo][i];
            r[ifo][i] -= rx[ifo][i];
         }
      }
   }

   switch(typ)
   {
         // Fixed dimension MCMC move
      case 1:
         if(sy > 0)
         {

            // subtract the current wavelet
            mx->wavelet(ty, paramsx, N, -1, Tobs);
            //printf("1:paramsx={%g,%g,%g,%g,%g}\n",paramsx[0],paramsx[1],paramsx[2],paramsx[3],paramsx[4]);
            mx->wavelet_bandwidth(paramsx, &fmin,&fmax);

            // add the new wavelet
            mx->wavelet(ty, paramsy, N, 1, Tobs);

            mx->wavelet_bandwidth(paramsy, &fi,&ff);
            //Make sure frequency is in range
            if(fi < fmin)
               fmin = fi;
            if(ff > fmax)
               fmax = ff;

            //Make sure frequency is in range
            if(fmin < data->fmin)
               fmin = data->fmin;
            if(fmax > data->fmax)
               fmax = data->fmax;

         }
         break;

         // RJMCMC birth move
      case 2:

         mx->wavelet(ty, paramsy, N, 1, Tobs);

         mx->wavelet_bandwidth(paramsy, &fmin,&fmax);
         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = data->fmin;
         if(fmax > data->fmax)
            fmax = data->fmax;

         break;

         // RJMCMC death move
      case 3:

         // subtract the current wavelet
         mx->wavelet(ty, paramsx, N, -1, Tobs);

         mx->wavelet_bandwidth(paramsx, &fmin,&fmax);
         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = data->fmin;
         if(fmax > data->fmax)
            fmax = data->fmax;

         break;

         //RJMCMC birth-death move
      case 4:

         //Some helper pointers
         if(det==-1) ifo = (int)floor(ran2(&chain->seed)*NI);

         struct Wavelet *wBirth = NULL;
         struct Wavelet *wDeath = NULL;

         if(det==-1)
         {
            wDeath = my->signal;
            wBirth = my->glitch[ifo];
         }
         else
         {
            wDeath = my->glitch[det];
            wBirth = my->signal;
         }

         // subtract the current wavelet from the signal model
         mx->wavelet(wDeath->templates, wDeath->intParams[ii], N, -1, Tobs);
         mx->wavelet_bandwidth(wDeath->intParams[ii], &fmin, &fmax);
         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = data->fmin;
         if(fmax > data->fmax)
            fmax = data->fmax;

         waveformProject(data,my->projection, my->extParams, ry, wDeath->templates, fmin, fmax);

         // where in glitch model do I put the new parameters?
         // find a label that isn't in use
         jj = 0;
         do
         {
            jj++;
            k = 0;
            for(j=0; j< wBirth->size; j++) if(jj == wBirth->index[j]) k = 1;
         } while(k == 1);

         wBirth->index[wBirth->size] = jj;

         wBirth->size++;

         // shift time of signal
         for(i=0; i<NW; i++) wBirth->intParams[jj][i] = wDeath->intParams[ii][i];
         if(det==-1)
         {
            wBirth->intParams[jj][0] += my->projection->deltaT[ifo];
            wBirth->intParams[jj][3] *= sqrt(my->projection->Fplus[ifo]*my->projection->Fplus[ifo]+my->projection->Fcross[ifo]*my->projection->Fcross[ifo]);
            wBirth->intParams[jj][4] = ran2(&chain->seed)*LAL_TWOPI;
         }
         else wBirth->intParams[jj][0] -= my->projection->deltaT[ifo];

         // add wavelet
         mx->wavelet(wBirth->templates, wBirth->intParams[jj], N, 1, Tobs);

         break;

      default:
         break;
   }
   //Make sure template array is zero if we have no wavelets in model
   if(sy == 0) for(i=0; i< N; i++) ty[i] = 0.0;

   // form up the resdial s-h
   if(det==-1 && data->signalFlag)
      waveformProject(data,my->projection, my->extParams, ry, ty, data->fmin, data->fmax);

  //try just computing deltaLogL for birth-death moves in glitch model
  if(typ==1 && data->intrinsic_likelihood == EvaluateMarkovianLogLikelihood)
  {

    if(data->stochasticFlag)
    {
      //TODO: No support for glitch model in stochastic likelihood
      ComputeNoiseCorrelationMatrix(data, mx->Snf, my->Sn, my->background);

      logLy = loglike_stochastic(NI, imin, imax, r, my->background->Cij, my->background->detCij);
    }
    else
    {

      logLy = 0.0;

      /*
       Compute logLy if we only change Sn:
       logLx = -(r|r)/2/Snx - NlogSnx
       logLy = -(r|r)/2/Sny - NlogSny
       -(r|r)/2 = (logLx + NlogSnx)*Snx
       so
       logLy = (logLx + NlogSnx)*(Snx/Sny) - NlogSny
       */
      for(ifo=0; ifo<NI; ifo++)
      {
        my->detLogL[ifo] = (mx->detLogL[ifo] + (imax-imin)*log(mx->Sn[ifo]))*(mx->Sn[ifo]/Sny[ifo]) -(imax-imin)*log(Sny[ifo]);
        logLy += my->detLogL[ifo];
      }

      if(sy > 0)
      {
        /*
         Now adjust logLy for changes in the glitch/signal parameters
         Keep the cost down by only computing deltaLogL in bins
         where modified wavelets have support
         */
        double fminx,fmaxx;
        double fminy,fmaxy;

        // subtract the current wavelet
        mx->wavelet_bandwidth(paramsx,&fminx,&fmaxx);

        // add the new wavelet
        mx->wavelet_bandwidth(paramsy,&fminy,&fmaxy);

        // make sure bandwidth spans both wavelets
        fmin = fminx;
        if(fminy<fmin) fmin = fminy;

        fmax = fmaxx;
        if(fmaxy>fmax) fmax = fmaxy;

        jmin = (int)floor(fmin*Tobs);
        jmax = (int)floor(fmax*Tobs);

        if(jmin<imin) jmin = imin;
        if(jmax>imax) jmax = imax;

        if(det==-1)
        {
          for(ifo=0; ifo<NI; ifo++)
          {
            //remove contribution between imin and imax of old model
            logLseg = loglike(jmin, jmax, r[ifo], Sny[ifo], invSnf[ifo]);
            my->detLogL[ifo] -= logLseg;
            logLy -= logLseg;

            //add contribution between imin and imax of new model
            for(i=jmin; i<jmax; i++)
            {
              j = 2*i;
              k = j+1;
              r[ifo][j] = s[ifo][j];
              r[ifo][k] = s[ifo][k];
              if(data->glitchFlag)
              {
                r[ifo][j] -= gy[ifo][j];
                r[ifo][k] -= gy[ifo][k];
              }
              if(data->signalFlag)
              {
                r[ifo][j] -= ry[ifo][j];
                r[ifo][k] -= ry[ifo][k];
              }
            }
            logLseg = loglike(jmin, jmax, r[ifo], Sny[ifo], invSnf[ifo]);
            my->detLogL[ifo] += logLseg;
            logLy += logLseg;
          }
        }
        else
        {
          //remove contribution between imin and imax of old model
          logLseg = loglike(jmin, jmax, r[det], Sny[det], invSnf[det]);
          my->detLogL[det] -= logLseg;
          logLy -= logLseg;

          //add contribution between imin and imax of new model
          for(i=jmin; i<jmax; i++)
          {
            j = 2*i;
            k = j+1;
            r[det][j] = s[det][j] - ry[det][j] - gy[det][j];
            r[det][k] = s[det][k] - ry[det][k] - gy[det][k];
          }
          logLseg = loglike(jmin, jmax, r[det], Sny[det], invSnf[det]);
          my->detLogL[det] += logLseg;
          logLy += logLseg;
        }
      }
    }
  }
  else if((typ==2 || typ==3) && data->intrinsic_likelihood == EvaluateMarkovianLogLikelihood)
  {
    if(typ==2)
      mx->wavelet_bandwidth(paramsy,&fmin,&fmax);
    else
      mx->wavelet_bandwidth(paramsx,&fmin,&fmax);

    for(ifo=0; ifo<NI; ifo++) my->detLogL[ifo] = mx->detLogL[ifo];
    logLy = mx->logL;
    jmin = (int)floor(fmin*Tobs);
    jmax = (int)floor(fmax*Tobs);

    if(jmin<imin) jmin = imin;
    if(jmax>imax) jmax = imax;

    if(det==-1)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        my->detLogL[ifo] = mx->detLogL[ifo];

        //remove contribution between imin and imax of old model
        logLseg = loglike(jmin, jmax, r[ifo], mx->Sn[ifo], invSnf[ifo]);
        my->detLogL[ifo] -= logLseg;
        logLy -= logLseg;

        //add contribution between imin and imax of new model
        for(i=jmin; i<jmax; i++)
        {
          j = 2*i;
          k = j+1;
          r[ifo][j] = s[ifo][j];// - ry[ifo][j] - gy[ifo][j];
          r[ifo][k] = s[ifo][k];// - ry[ifo][k] - gy[ifo][k];
          if(data->glitchFlag)
          {
            r[ifo][j] -= gy[ifo][j];
            r[ifo][k] -= gy[ifo][k];
          }
          if(data->signalFlag)
          {
            r[ifo][j] -= ry[ifo][j];
            r[ifo][k] -= ry[ifo][k];
          }
        }
        logLseg = loglike(jmin, jmax, r[ifo], Sny[ifo], invSnf[ifo]);
        my->detLogL[ifo] += logLseg;
        logLy += logLseg;
      }
    }
    else
    {
      my->detLogL[det] = mx->detLogL[det];

      //remove contribution between imin and imax of old model
      logLseg = loglike(jmin, jmax, r[det], mx->Sn[det], invSnf[det]);
      my->detLogL[det] -= logLseg;
      logLy -=  logLseg;

      //add contribution between imin and imax of new model
      for(i=jmin; i<jmax; i++)
      {
        j = 2*i;
        k = j+1;
        r[det][j] = s[det][j] - ry[det][j] - gy[det][j];
        r[det][k] = s[det][k] - ry[det][k] - gy[det][k];
      }
      logLseg = loglike(jmin, jmax, r[det], Sny[det], invSnf[det]);
      my->detLogL[det] += logLseg;
      logLy += logLseg;
    }
  }
  else
  {
    for(ifo=0; ifo<NI; ifo++)
    {
      for(i=0; i< N; i++)
      {
        r[ifo][i] = s[ifo][i];
        //if(data->signalFlag || data->glitchFlag) r[ifo][i] -= ry[ifo][i] + gy[ifo][i];
        if(data->glitchFlag)
        {
          r[ifo][i] -= gy[ifo][i];
        }
        if(data->signalFlag)
        {
          r[ifo][i] -= ry[ifo][i];
        }
      }
      my->detLogL[ifo] = loglike(imin, imax, r[ifo], Sny[ifo], invSnf[ifo]);
      logLy += my->detLogL[ifo];
    }
  }

  free(gx);
  free(gy);

  return logLy;

}

double EvaluateExtrinsicConstantLogLikelihood(UNUSED struct Network *projection, double *params, UNUSED double **invSnf, UNUSED double *Sn, UNUSED double *geo, UNUSED double **g, UNUSED struct Data *data, UNUSED double fmin, UNUSED double fmax)
{
   if(extrinsic_checkrange(params)) return -1.0e60;

   else return 0.0;
}

double EvaluateExtrinsicMarkovianLogLikelihood(struct Network *projection, double *params, double **invSnf, double *Sn, double *geo, double **g, struct Data *data, double fmin, double fmax)
{
   int i, n;
   int NI,N;
   int ifo;
   int imin,imax;

   //double sum = 0.0;
   double logL = 0.0;
   double **h, **d, **r;

   if(extrinsic_checkrange(params)) return -1.0e60;

   NI   = data->NI;
   N = data->N;
   //imin = data->imin;
   //imax = data->imax;
   imin = (int)(fmin*data->Tobs);
   imax = (int)(fmax*data->Tobs);

   d   = data->s;
   h   = dmatrix(0,NI-1,0,N-1); //template
   r   = dmatrix(0,NI-1,0,N-1); //residual


   //initialize template and residual to 0's
   for(i=0; i<NI; i++) for(n=0; n<N; n++) h[i][n] = 0.0;

   //compute instrument response h[] to geocenter waveform geo[]
   computeProjectionCoeffs(data, projection, params, fmin, fmax);
   waveformProject(data, projection, params, h, geo, fmin, fmax);
   
   
  //Form up residual
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N; n++)
    {
      r[ifo][n] = d[ifo][n];
      if(data->signalFlag) r[ifo][n] -= h[ifo][n];
      if(data->glitchFlag) r[ifo][n] -= g[ifo][n];
    }
    logL += loglike(imin, imax, r[ifo], Sn[ifo], invSnf[ifo]);
  }


   free_dmatrix(h,0,NI-1,0,N-1);
   free_dmatrix(r,0,NI-1,0,N-1);
   
   return logL;
}

double EvaluateExtrinsicSearchLogLikelihood(struct Network *projection, double *params, double **invSnf, double *Sn, double *geo, double **g, struct Data *data, double fmin, double fmax)
{
   int i, n;
   int NI,N;
   int ifo;
   int imin,imax;
   
   double Tobs;
   double logL = 0.0;
   double dt=0,dphi=0,dA=1;
   double **h, **d, **r;
   
   if(extrinsic_checkrange(params)) return -1.0e60;
   
   NI   = data->NI;
   N = data->N;
   Tobs = data->Tobs;
   imin = (int)floor(fmin*data->Tobs);//data->imin;
   imax = (int)floor(fmax*data->Tobs);//data->imax;
   
   d   = data->s;
   h   = dmatrix(0,NI-1,0,N-1); //template
   r   = dmatrix(0,NI-1,0,N-1); //residual   
   
   //initialize template and residual to 0's
   for(i=0; i<NI; i++) for(n=0; n<N; n++) h[i][n] = 0.0;
   
   //compute instrument response h[] to geocenter waveform geo[]
   computeProjectionCoeffs(data, projection, params, data->fmin, data->fmax);
   waveformProject(data, projection, params, h, geo, data->fmin, data->fmax);
   
   //Form up residual
   for(ifo=0; ifo<NI; ifo++)
   {
      for(n=0; n<N; n++)
      {
         r[ifo][n] = d[ifo][n];
         if(data->signalFlag) r[ifo][n] -= h[ifo][n];
         if(data->glitchFlag) r[ifo][n] -= g[ifo][n];
      }
   }

   //Amplitude
   params[1] *= dA;
   
   //Time
   params[5] += dt;
   
   //Phase
   params[6] += dphi;
   
   if(params[6] < 0.0)       params[6] += LAL_TWOPI;
   if(params[6] > LAL_TWOPI) params[6] -= LAL_TWOPI;
   
   if(params[5] < 0.0)  params[5] += Tobs;
   if(params[5] > Tobs) params[5] -= Tobs;
   
   // check that we haven't mapped out of range
   if(extrinsic_checkrange(params))
   {
       free_dmatrix(h,0,NI-1,0,N-1);
       free_dmatrix(r,0,NI-1,0,N-1);
       return -1.0e60;
   }
   
   //Now update the full model with current extrinsic parameters
   computeProjectionCoeffs(data, projection, params, data->fmin, data->fmax);
   waveformProject(data, projection, params, h, geo, data->fmin, data->fmax);
   //Form up residual
   for(ifo=0; ifo<NI; ifo++)
   {
      for(n=0; n<N; n++)
      {
         r[ifo][n] = d[ifo][n];
         if(data->signalFlag) r[ifo][n] -= h[ifo][n];
         if(data->glitchFlag) r[ifo][n] -= g[ifo][n];
      }
   }
   logL = 0.0;
   for(ifo=0; ifo<NI; ifo++) logL += loglike(imin, imax, r[ifo], Sn[ifo], invSnf[ifo]);
   
   free_dmatrix(h,0,NI-1,0,N-1);
   free_dmatrix(r,0,NI-1,0,N-1);
   
   return logL;
}
