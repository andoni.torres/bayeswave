
void set_bayesline_priors(char *channel, struct BayesLineParams *bayesline, double Tobs);

int checkrange(double *p, double **r, int NW);

int extrinsic_checkrange(double *p);

void proximity_normalization(double **params, double **range, double *norm, int *larray, int cnt);
double wavelet_proximity_density(double f, double t, double **params, int *larray, int cnt, struct Prior *prior);
double wavelet_proximity_prior(struct Wavelet *wave, struct Prior *prior);

double glitch_background_prior(struct Prior *prior, double *params);

double signal_amplitude_prior (double *params, double *Snf, double Tobs, double SNRpeak);
double glitch_amplitude_prior (double *params, double *Snf, double Tobs, double SNRpeak);
double uniform_amplitude_prior(double *params, double *Snf, double Sa, double Tobs, double **range);

//cluster prior normalization
/*static double cluster_norm_flow16_twin1_srate1024_Qmax40[41] =
{
    9696.64205,
    9654.93967,
    9352.44057,
    8243.25708,
    7982.28457,
    7169.58427,
    6802.25120,
    6349.58660,
    5665.19218,
    5339.00748,
    4809.94170,
    4614.02412,
    4167.94425,
    3912.01129,
    3906.98062,
    3496.42005,
    3275.34256,
    2997.71595,
    2817.77938,
    2585.26401,
    2372.35131,
    2262.66725,
    2070.40905,
    1909.33683,
    1798.24863,
    1610.84150,
    1617.39090,
    1460.61370,
    1333.14552,
    1285.56563,
    1149.59129,
    1136.63216,
    1023.32365,
    931.23604,
    844.43683,
    803.73284,
    757.99277,
    713.94503,
    650.09965,
    622.82687,
    622.82687
};

 static double cluster_norm_flow16_twin1_srate512_Qmax40[41] =
 {
     9044.89878,
     9546.68985,
     9037.81568,
     7796.71104,
     7281.62844,
     6520.58316,
     6338.81494,
     6104.98532,
     5531.94993,
     5092.09940,
     4989.29308,
     4471.26723,
     4365.62166,
     3982.43388,
     3705.11198,
     3531.19527,
     3341.72265,
     3100.39755,
     2879.00995,
     2664.83807,
     2524.97061,
     2306.22963,
     2150.91746,
     2004.44453,
     1854.29840,
     1671.82396,
     1552.79544,
     1419.67595,
     1359.68838,
     1267.29581,
     1188.19865,
     1109.90591,
     1026.88085,
     921.23236,
     889.75423,
     824.20547,
     755.59226,
     707.69053,
     675.11090,
     595.37429,
     595.37429
 };*/
